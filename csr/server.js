const express = require('express')
const app = express()
const path = require('path')

// With this line, our server will know how to parse any incoming request
// that contains some JSON in the body
app.use(express.json())

// Our fake database 
const catList = [
    {
        name: "Cat 1",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 2",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 4",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
]

// HTTP GET api that returns all the cats in our fake database
app.get("/api/cats", (req, res) => {
    return res.json(catList)
})

// HTTP POST apio that will push (and therefore create) a new element in 
// our fake database 
app.post("/api/cats", (req, res) => {
    const { body } = req
    catList.push(body)
    return res.sendStatus(200)
})

// Whatever request gets here, it will be redirected to serve a static file
// from the client folder
app.use(express.static(path.join(__dirname, "client")))

app.listen(3000, () => {
    console.log("Server running on port 3000")
})