# Lesson 10 

In this lesson we saw different ways of rendering the content of our webpage. You will find 3 folders:

- `csr`: Client-Side Rendering, with a fake in-memory database where we are retrieving our content. We are requesting ONLY DATA from our frontend
- `ssr`: Server-Side Rendering, with a fake in-memory database. We are retrieving the FULL already-rendered page from our server.
- `csr-db`: Client-Side Rendering with an actual database.

### How to setup your database for local development

Here are the instruction of how to setup the database for you local development. First of all, you need the following software installed on your pc: 

- PostgreSQL: [https://www.postgresql.org/](https://www.postgresql.org/)
- PgAdmin4: [https://www.pgadmin.org/](https://www.pgadmin.org/)

When installing postgresql, please keep the password you are using as simple as possible, since it's only for local development. The default user will be `postgres`, so I usually pick `postgres` also for password.

### Setup pgAdmin

To create your database, open first PgAdmin. From the left list, right click on "Servers". 

![](https://fs.i3lab.group/hypermedia/images/1.png)

We need to Register a new server. 

![](https://fs.i3lab.group/hypermedia/images/2.png)

Fill this popup with the informations to connect to Postgres

![](https://fs.i3lab.group/hypermedia/images/3.png)

Then, when a "server" connection is created, you should have a screen like the following one
![](https://i.stack.imgur.com/rcFhA.png)

### Install modules inside the project

Next we need to install the correct packages/modules in our project. To do so, we have to run the following commands:

```bash
npm install sequelize pg pg-hstore
```

This will install our ORM `sequelize`, and the depedencies we need to use for PostgreSQL which are `pg` and `pg-hstore`.

### Adding a connection in our server code

Inside our `server.js` file, we need to add a connection to the database. To do so, we create a new variable which contain the identification data for our local database. In my example:

```js
const database = new Sequelize("postgres://postgres:postgres@localhost:5432/hyp")
```

From left to right:
- the first `postgres` is the protocol used
- the second `postgres` is the default username
- the third `postgres` is the password I'm using to connect
- `localhost:5432` is the location of my service in my pc
- `hyp` is the name of the database I'm creating


To check that everything is working correctly, just add the following like of code before `app.listen(3000)`:

```js
database.authenticate()
```

and once you run the server, you should have a log saying 

```
Executing (default): SELECT 1+1 AS result
```

Now you can follow along with the rest of the code :)