const express = require('express')
const app = express()
const path = require('path')
const { Sequelize, DataTypes } = require('sequelize')

// With this line, our server will know how to parse any incoming request
// that contains some JSON in the body
app.use(express.json())

// Creating the object that will connect the Node application to our database
const database = new Sequelize("postgres://postgres:postgres@localhost:5432/hyp")


// Function that will initialize the connection to the database
async function initializeDatabaseConnection() {
    // Step 1: Authenticating 
    await database.authenticate()
    // Step 2: Definition of the tables we want in our DB
    // (you can find more here https://sequelize.org/v6/manual/model-basics.html)
    const Cat = database.define("cat", {
        name: DataTypes.STRING,
        breed: DataTypes.STRING,
        img: DataTypes.STRING,
    })
    // Step 3: Synchronize the changes with the actual database
    // If you use the force: true option, you will be WIPING the whole database
    // every time you restart the server. During the development this is fine.
    // When you release your website, definitely not.
    await database.sync({ force: true })
    // We return an object with models we can use
    return {
        Cat
    }
}

async function startApplicationServer() {
    // Here we will use the models
    const models = await initializeDatabaseConnection()
    // Like in creating a new cat in our database at the initialization
    await models.Cat.create({
        name: "Cat something",
        breed: "Breed something",
        img: "https://fs.i3lab.group/hypermedia/cats/siberian.jpg",
    })

    app.get("/api/cats", async (req, res) => {
        // .findAll() is used to retrieve ALL the cats in our database
        // if we want to filter it more, we need some options
        // More on that here https://sequelize.org/v6/manual/model-querying-basics.html
        const catList = await models.Cat.findAll()
        return res.json(catList)
    })

    app.post("/api/cats", (req, res) => {
        const { body } = req
        // Here we create the cat and add it immediatly to the database
        const cat = await models.Cat.create(body)
        return res.status(200).json(cat)
    })

    app.use(express.static(path.join(__dirname, "client")))
}






app.listen(3000, () => {
    console.log("Server running on port 3000")
    startApplicationServer()
})