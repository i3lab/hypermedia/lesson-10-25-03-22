
// Retrieve the title TAG
const title = document.getElementById("title")
// Get the content inside
const content = title.innerHTML
// Print it
console.log(content)

// Update the content of the h1 element
title.innerHTML = "The cat shelter Edited"

// Function that returns a string containing the template that we are going
// to append to the main id
function createCatTemplate(name, breed, img) {
    return `<div class="card">
        <div class="image-container">
            <img class="cat-img" src="${img}" />
        </div>
        <span class="cat-name">${name}</span>
        <span class="cat-breed">${breed}</span>
    </div>`
}

// Retrieve the TAG main of our page
const app = document.getElementById('app')

// Function used to request ONLY the data from our server as an array of cats
async function requestCats() {
    const data = await fetch("http://localhost:3000/api/cats")
    // Here we convert the data in a json format
    const arrOfCats = await data.json()
    for (let cat of arrOfCats) {
        app.innerHTML += createCatTemplate(cat.name, cat.breed, cat.img)
    }
}


// Call the function to execute it when the page is loaded
requestCats()

// Function called by the button in the index.html page
async function parseInput() {
    const boxName = document.getElementById("name")
    const boxBreed = document.getElementById("breed")
    const boxImage = document.getElementById("img")
    const nameValue = boxName.value
    const breedValue = boxBreed.value
    const imgValue = boxImage.value
    const payload = {
        name: nameValue,
        breed: breedValue,
        img: imgValue
    }
    // Here we make the POST request in order to create a new cat with
    // the info that we retrieved from our 3 inputs
    await fetch("http://localhost:3000/api/cats", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        // we need here to make it a string in order to send it to our server
        body: JSON.stringify(payload)
    })
}

