
// Retrieve the title TAG
const title = document.getElementById("title")
// Get the content inside
const content = title.innerHTML
// Print it
console.log(content)

// Update the content of the h1 element
title.innerHTML = "The cat shelter Edited"


// Our list of cats :)

// Retrieve the TAG main of our page
const app = document.getElementById('app')

// This is not required in server side rendering, since our page will arrive
// ALREADY rendered

// async function requestCats() {
//     const data = await fetch("http://localhost:3000/api/cats")
//     const arrOfCats = await data.json()

//     console.log(arrOfCats)

//     for (let cat of arrOfCats) {
//         app.innerHTML += createCatTemplate(cat.name, cat.breed, cat.img)
//     }
// }


// requestCats()

// Function called by the button in the index.html page
async function parseInput() {
    const boxName = document.getElementById("name")
    const boxBreed = document.getElementById("breed")
    const boxImage = document.getElementById("img")
    const nameValue = boxName.value
    const breedValue = boxBreed.value
    const imgValue = boxImage.value
    const payload = {
        name: nameValue,
        breed: breedValue,
        img: imgValue
    }
    await fetch("http://localhost:3000/api/cats", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(payload)
    })
    // app.innerHTML += createCatTemplate(nameValue, breedValue, imgValue)
}

