const express = require('express')
const app = express()
const path = require('path')

app.use(express.json())

// Function that returns a string containing the template that we are going
// to append to insert in the result
function createCatTemplate(name, breed, img) {
    return `<div class="card">
        <div class="image-container">
            <img class="cat-img" src="${img}" />
        </div>
        <span class="cat-name">${name}</span>
        <span class="cat-breed">${breed}</span>
    </div>`
}

// Our fake database
const catList = [
    {
        name: "Cat 1",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 2",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
    {
        name: "Cat 4",
        breed: "Siberian",
        img: "img/siberian.jpg",
    },
]

app.post("/api/cats", (req, res) => {
    const { body } = req
    catList.push(body)
    return res.sendStatus(200)
})

app.get("/", (req, res) => {
    let catsString = ""
    for (const cat of catList) {
        catsString += createCatTemplate(cat.name, cat.breed, cat.img)
    }
    return res.send(`
<!DOCTYPE html>
<html>

<head>
  <title>The cat shelter</title>
  <link rel="stylesheet" href="my-css.css" />
</head>

<body>
  <header>
    <h1 id="title">The cat shelter</h1>
  </header>
  <div class="custom-form">
    <input placeholder="Name" type="text" id="name">
    <input placeholder="Breed" type="text" id="breed">
    <input placeholder="Image" type="text" id="img">
    <button onclick="parseInput()">Save</button>
  </div>
  <main id="app">
  ${catsString}
  </main>
  <footer>Made by me :)</footer>
</body>

<script src="index.js"></script>

</html>
`)
})

app.use(express.static(path.join(__dirname, "client")))

app.listen(3000, () => {
    console.log("Server running on port 3000")
})